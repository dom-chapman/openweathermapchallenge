//
//  OpenWeatherTestCase.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import XCTest

class OpenWeatherTestCase: XCTestCase {
    
    /**
     Returns the contents of the local json file
     - parameter name: String
     - returns: Data
     */
    public func getTestData(forFileName name: String) -> Data? {
        
        // local json file
        let bundle = Bundle(for: OpenWeatherTestCase.self)
        
        guard let path = bundle.url(forResource: name, withExtension: "json") else {
            assertionFailure("Failed to locate local test file")
            return nil
        }
        
        do {
            return try Data(contentsOf: path)
            
        } catch {
            print("There was an error locating the test.json file: \(error.localizedDescription)")
            return nil
        }
    }
}
