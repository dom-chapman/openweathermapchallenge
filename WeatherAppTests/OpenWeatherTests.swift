//
//  OpenWeatherTests.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import XCTest
@testable import WeatherApp

class OpenWeatherTests: OpenWeatherTestCase {
    
    func testRequestOpenWeather() {
        let requestWeatherDataExpectation = self.expectation(description: "Should successfully request data from OpenWeatherMap API")
        
        OpenWeather.requestOpenWeather(forCity: "London") { (city) in
            XCTAssertNotNil(city, "Failed to parse OpenWeatherMap API response data")
            requestWeatherDataExpectation.fulfill()
        }
        
        self.waitForExpectations(timeout: 1.0, handler: { (error) in
            if error != nil { XCTFail("Completion closure was never called before timeout")  }
        })
    }
    
    func testRequestOpenWeatherForCity() {
        let testData = self.getTestData(forFileName: "TestWeatherData")
        
        // parse city data
        let parseExpectation = self.expectation(description: "Shoud successfully parse test OpenWeatherMap API data")
        
        var city: OWCity!
        
        OpenWeather.parseOpenWeatherCity(withData: testData, completion: { (owCity) in
            XCTAssertNotNil(owCity, "Failed to parse data")
            city = owCity
            parseExpectation.fulfill()
        })
        
        self.waitForExpectations(timeout: 1.0, handler: { (error) in
            if error != nil { XCTFail("Completion closure was never called before timeout")  }
        })
        
        // test OWCity properties
        XCTAssertEqual(city.ID, 2643743, "Failed to correctly map the city ID")
        XCTAssertEqual(city.name, "London", "Failed to correctly make the name of the city")
        XCTAssertEqual(city.weatherForecast.count, 2, "Failed to map the correct number of OWWeatherForecast for this OWCity")
        
        // test the OWWeatherForecast properties
        guard let forecast = city.weatherForecast.first else {
            XCTFail("Failed to unwrap weather forecast for city")
            return
        }
        
        // test the OWWeather properties
        XCTAssertNotNil(forecast.dateTimestamp, "Failed to map date from timestamp")
        XCTAssertEqual(Date.formatWeatherDate(date: forecast.dateTimestamp!), "Friday", "Failed to correctly map the timestamp for the weather forecast")
        
        XCTAssertNotNil(forecast.temperature, "Failed to map weather forecast temperature")
        XCTAssertEqual(forecast.temperature?.day, Float(13.15), "Failed to map day temperature correctly")
        XCTAssertEqual(forecast.temperature?.night, Float(8.5), "Failed to map night temperature correctly")
        XCTAssertEqual(forecast.temperature?.morn, Float(10.41), "Failed to map morning temperature correctly")
        XCTAssertEqual(forecast.temperature?.eve, Float(12.11), "Failed to map evening temperature correctly")
        XCTAssertEqual(forecast.temperature?.min, Float(8.5), "Failed to map minimum temperature correctly")
        XCTAssertEqual(forecast.temperature?.max, Float(13.44), "Failed to map maximum temperature correctly")
        
        XCTAssertEqual(forecast.weather.count, 1, "Failed to map the correct number of OWWweather objects")
        XCTAssertEqual(forecast.weather.first!.id, 500)
        XCTAssertNotNil(forecast.weather.first!.main, "Failed to map weather main property")
        XCTAssertEqual(forecast.weather.first!.main!, "Rain", "Failed to map weather main property")
        XCTAssertNotNil(forecast.weather.first!.weatherDescription, "Failed to map weather description property")
        XCTAssertEqual(forecast.weather.first!.weatherDescription!, "light rain", "Failed to map weather description property")
        XCTAssertNotNil(forecast.weather.first!.icon, "Failed to map weather icon property")
        XCTAssertEqual(forecast.weather.first!.icon!, "10d", "Failed to map weather icon property")
    }
}
