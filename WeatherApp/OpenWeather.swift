//
//  OpenWeather.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import Alamofire

class OpenWeather {
    
    // MARK: - Weather Forecast Request
    
    /**
     Performs a request to OpenWeatherMap API for weather data for the next 7 days for a given city. 
     - parameter city: String
     - parameter completion: closure returning OWCity object which is parsed OpenWeatherMap API data
     */
    public static func requestOpenWeather(forCity city: String, completion: @escaping (_ city: OWCity?) -> Void) {
        
        let requestParameters = ["q": city,
                                 "APPID": "555725d1576d29544368acb37925d760",
                                 "mode": "JSON",
                                 "cnt": "7",
                                 "units": "metric"]
        
        // perform API request
        Alamofire.request("http://api.openweathermap.org/data/2.5/forecast/daily", parameters: requestParameters).response { response in
            self.parseOpenWeatherCity(withData: response.data, completion: completion)
        }
    }
    
    /**
     Parses OpenWeatherMap API data
     - parameter data: Data
     - parameter completion: closure containing optional parsed OWCity object
     */
    public static func parseOpenWeatherCity(withData data: Data?, completion: @escaping (_ city: OWCity?) -> Void) {
        guard let weatherData = data,
            let serializedWeatherData = self.serializeData(data: weatherData) else {
                completion(nil)
                return
        }
        
        // parse city data
        guard let city = serializedWeatherData["city"] as? [String: AnyObject],
            let id: Int = city[OWCity.AttributeReference.id] as? Int,
            let name: String = city[OWCity.AttributeReference.name] as? String else {
                completion(nil)
                return
        }
        
        // parse open weather data in background thread
        DispatchQueue.global(qos: .userInitiated).async {
            let owCity = OWCity(withID: id, name: name)
            
            // parse weather data
            guard let forecast = serializedWeatherData["list"] as? Array<[String: AnyObject]> else {
                DispatchQueue.main.async {
                    completion(owCity)
                }
                return
            }
            
            // parse weather forecast data
            for day in forecast {
                let weatherForecast = OWWeatherForecast()
                
                // date of weather forecast
                if let date = day[OWWeatherForecast.AttributeReference.timestamp] as? Int {
                    weatherForecast.dateTimestamp = Date(timeIntervalSince1970: TimeInterval(date))
                }
                
                // temperature of weather forecast
                if let temp = day[OWWeatherForecast.AttributeReference.temperature] as? [String: AnyObject] {
                    let day = temp[OWTemperature.AttributeReference.day] as? Float
                    let night = temp[OWTemperature.AttributeReference.night] as? Float
                    let morn = temp[OWTemperature.AttributeReference.morn] as? Float
                    let eve = temp[OWTemperature.AttributeReference.eve] as? Float
                    let min = temp[OWTemperature.AttributeReference.min] as? Float
                    let max = temp[OWTemperature.AttributeReference.max] as? Float
                    weatherForecast.temperature = OWTemperature(withDay: day, night: night, morn: morn, eve: eve, min: min, max: max)
                }
                
                // weather information for forecast
                if let weather = day[OWWeatherForecast.AttributeReference.weather] as? Array<[String: AnyObject]> {
                    for weatherItem in weather {
                        let id = weatherItem[OWWeather.AttributeReference.id] as? Int
                        let main = weatherItem[OWWeather.AttributeReference.main] as? String
                        let weatherDescription = weatherItem[OWWeather.AttributeReference.weatherDescription] as? String
                        let icon = weatherItem[OWWeather.AttributeReference.icon] as? String
                        
                        let owWeather = OWWeather(withID: id, main: main, description: weatherDescription, icon: icon)
                        weatherForecast.weather.append(owWeather)
                    }
                }
                
                owCity.weatherForecast.append(weatherForecast)
            }
            
            // call completion closure on main thread
            DispatchQueue.main.async {
                completion(owCity)
            }
        }
    }
    
    // MARK: - JSON Serialization
    
    /**
     Serializes Data
     - returns: Array of Dictionaries
     */
    private static func serializeData(data: Data) -> [String: AnyObject]? {
        do {
            let result = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            
            if let data = result as? [String: AnyObject] {
                return data
            }
            
        } catch let error {
            print("There was an error with JSON Serialization: \(error.localizedDescription)")
            return nil
        }
        
        return nil
    }
}
