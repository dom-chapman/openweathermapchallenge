//
//  StringExtension.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation

extension String {
    static let Empty = ""
    
    public static func degreesFormat(forTemperature temp: Float) -> String {
        // remvove decimal places for cleaner display
        let tempCleaned = String(format: "%.0f", temp)
        return "\(tempCleaned)◦"
    }
}
