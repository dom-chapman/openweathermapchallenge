//
//  UIFontExtension.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    private static func helveticaLightFont(withSize size: CGFloat) -> UIFont? {
        return UIFont(name: "HelveticaNeue-Light", size: size)
    }
    
    public static func weatherLargeFont() -> UIFont? {
        return self.helveticaLightFont(withSize: 17)
    }
    
    public static func weatherCellDateFont() -> UIFont? {
        return self.helveticaLightFont(withSize: 16)
    }
    
    public static func weatherCellTempFont() -> UIFont? {
        return self.helveticaLightFont(withSize: 14)
    }
    
    public static func weatherCellDescriptionFont() -> UIFont? {
        return self.helveticaLightFont(withSize: 15)
    }
}
