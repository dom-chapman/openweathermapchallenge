//
//  DateExtension.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation

extension Date {
    public static func formatWeatherDate(date: Date) -> String {
        return DateFormatter.weatherDateFormatter.string(from: date)
    }
}
