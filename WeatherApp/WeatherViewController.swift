//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var cityNameLabel: UILabel? {
        didSet {
            self.cityNameLabel?.font = UIFont.weatherLargeFont()
            self.cityNameLabel?.text = String.Empty
        }
    }
    
    @IBOutlet weak var requestWeatherTextField: UITextField? {
        didSet {
            self.requestWeatherTextField?.font = UIFont.weatherLargeFont()
            self.requestWeatherTextField?.returnKeyType = .done
            self.requestWeatherTextField?.delegate = self
        }
    }
    
    @IBOutlet weak var requestWeatherButton: UIButton?
    
    @IBOutlet weak var requestActivityIndicator: UIActivityIndicatorView? {
        didSet {
            self.requestActivityIndicator?.isHidden = true
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView?
    
    lazy var dataSource = WeatherViewControllerDataSource()

    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // collection view setup
        self.collectionView?.dataSource = self.dataSource
        self.collectionView?.register(UINib(nibName: "WeatherCell", bundle: nil), forCellWithReuseIdentifier: "WeatherCell")
    }
    
    // MARK: - Request Weather
    
    /**
     UIButton action for requesting weather
     - parameter button: UIButton
     */
    @IBAction func requestWeather(button: UIButton) {
        self.requestWeather(forLocation: self.requestWeatherTextField?.text)
    }
    
    /**
     Initiates request for weather api data from OpenWeatherMap for a given location
     - parameter location: String
     */
    fileprivate func requestWeather(forLocation location: String?) {
        guard let location = location, !location.isEmpty else {
            return
        }
        
        // update loading state of view
        self.showLoading()
        
        // request weather data
        OpenWeather.requestOpenWeather(forCity: location) { [weak self] (openWeatherCity) in
            self?.requestActivityIndicator?.stopAnimating()
            
            guard let city = openWeatherCity else {
                return
            }
            
            // reload data
            self?.cityNameLabel?.text = city.name
            self?.dataSource.update(withWeatherForecasts: city.weatherForecast)
            self?.collectionView?.reloadData()
        }
    }
    
    // MARK: - Update
    
    /**
     Update UI to show loading of weather data
     */
    private func showLoading() {
        // clear all current results from datasource
        self.dataSource.update(withWeatherForecasts: [])
        self.collectionView?.reloadData()
        
        // animate activity indicator
        self.requestActivityIndicator?.isHidden = false
        self.requestActivityIndicator?.startAnimating()
        
        // simple animation of fading out ui elements
        UIView.animate(withDuration: 0.25, animations: { 
            self.requestWeatherButton?.alpha = 0
            self.requestWeatherTextField?.alpha = 0
        }) { (completion) in
            self.requestWeatherButton?.isHidden = true
            self.requestWeatherTextField?.isHidden = true
        }
    }
}

extension WeatherViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.requestWeather(forLocation: textField.text)
        return true
    }
}

