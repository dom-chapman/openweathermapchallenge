//
//  WeatherViewControllerDataSource.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit

class WeatherViewControllerDataSource: NSObject, UICollectionViewDataSource {
    
    private var forecasts = [OWWeatherForecast]()
    
    // MARK: - Weather
    
    /**
     Updates local property for weather forecast array
     - parameter forecasts: OWWeatherForecast array
     */
    public func update(withWeatherForecasts forecasts: [OWWeatherForecast]) {
        // remove all current weather forecast objects
        self.forecasts.removeAll()
        
        // add new weather forecast objects
        self.forecasts.append(contentsOf: forecasts)
    }
    
    // MARK: - UICollectionView DataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.forecasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let forecast = self.forecasts[section]
        return forecast.weather.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherCell", for: indexPath) as? WeatherCell {
            cell.update(withWeatherForecast: self.weatherForecastForIndexPath(indexPath: indexPath))
            return cell
        }
        
        assertionFailure("Failed to identify correct collection view cell")
        return UICollectionViewCell()
    }
    
    // MARK: - Convenience

    /**
     Returns the date property for a OWWeatherForecast object at a given IndexPath
     - parameter indexPath: IndexPath
     - returns: optional Date
     */
    private func dateForIndexPath(indexPath: IndexPath) -> Date? {
        guard indexPath.section <= self.forecasts.count else {
            return nil
        }
        
        return self.forecasts[indexPath.section].dateTimestamp
    }
    
    /**
     Returns the OWWeatherForecast object for a given IndexPath
     - parameter indexPath: IndexPath
     - returns: optional OWWeatherForecast
     */
    private func weatherForecastForIndexPath(indexPath: IndexPath) -> OWWeatherForecast? {
        guard indexPath.section <= self.forecasts.count else {
            return nil
        }
        
        return self.forecasts[indexPath.section]
    }
    
    /**
     Returns the OWWeather object at a given IndexPath
     - parameter indexPath: IndexPath
     - returns: optional OWWeather
     */
    private func weatherForIndexPath(indexPath: IndexPath) -> OWWeather? {
        // check desired weather forecast is not out of range
        guard indexPath.section <= self.forecasts.count else {
            return nil
        }
        
        // weather forecast
        let forecast = self.forecasts[indexPath.section]
        
        // check desired weather is not out of range
        guard indexPath.row <= forecast.weather.count else {
            return nil
        }
        
        return forecast.weather[indexPath.row]
    }
}
