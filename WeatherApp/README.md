
# Dominic Chapman - Task

This is my code for the Open Weather Map App Challenge. As requested, the app allows the user to search for a given location and requests the weather for the next 7 days. The data is displayed in collection view with a custom layout.

### Third party libraries
Alamofire is the only framework added to the app and is added using the dependency manager cocoapods. Alamofire has been added to the app to utilise the already established and tested functionality of making URL requests. 
When running the app for the first time, an update of cocoapods is required.

### Basic architecture overview
When the app is launched, the WeatherViewController is presented to the user with UI elements allowing the input of a city name.
When a request for weather data is made by the user, the WeatherViewController invokes a static function within OpenWeather.swift. This uses Alamofire to make a request to the OpenWeatherMap API for weather forecast data for the user's provided city.   
This is then parsed and returned to the WeatherViewController to then be displayed.

### Custom collection view layout
The custom collection view layout has been designed to display collection view cells which fit the width of the collection view  and are displayed at a set height.

Given more time on this project, I would improve the collection view layout to include a delegate allowing custom heights to be set for collection view section headers and cells.

## Unit Tests
Unit tests have been added to test the core data import functionality and object mapping within the app. A custom test case class (OpenWeatherTestCase) has also been added for loading local test JSON data. If more time were available for this project, I would want to improve the overall unit test coverage of the app.

## Future updates

### Data mapping
Currently not all of the OpenWeatherMap API data is being mapped into the data models. Data such as humidity, pressure and chances of rain. Given more time, adding all of the data would allow for more detail to be included within the collection view cells. 

### Pull to refresh
There is no method to allow the user to refresh the request for weather data and this could be achieved by implementing a pull to refresh.

### Weather Requests
The current user journey allows for a search to be completed once before showing the user the search results. There is no functionality built into the app to allow for a new search to be  completed. Given more time this could be a simple addition by including a UIBarButtonItem in a UINavigationBar to refresh the UI and clear the search results in the collection view datasource.

### Data Storage
Search results are not stored within the app and although weather data changes regularly, caching the data offline for a short period of time might be a useful addition for the user.

### Error Handling
Currently there is no error state or empty search results state displayed to the user when a search completes with no search results. Given more time, I would prioritise this feature along with the option to allow the user to make another weather forecast request.
