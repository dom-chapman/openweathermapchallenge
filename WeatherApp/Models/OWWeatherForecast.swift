//
//  OWWeatherForecast.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit

class OWWeatherForecast: NSObject {
    var dateTimestamp: Date? = nil
    var temperature: OWTemperature? = nil
    var weather = [OWWeather]()
    
    public struct AttributeReference {
        static let timestamp = "dt"
        static let temperature = "temp"
        static let weather = "weather"
    }
}
