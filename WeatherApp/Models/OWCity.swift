//
//  OWCity.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit

class OWCity: NSObject {
    var ID: Int? = nil
    var name: String? = nil
    var weatherForecast = [OWWeatherForecast]()
    
    public struct AttributeReference {
        static let id = "id"
        static let name = "name"
    }
    
    convenience init(withID id: Int, name: String) {
        self.init()
        
        self.ID = id
        self.name = name
    }
}
