//
//  OWWeather.swift
//  WeatherApp
//
//  Created by Dom Chapman on 10/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation

class OWWeather: NSObject {
    var id: Int? = nil
    var main: String? = nil
    var weatherDescription: String? = nil
    var icon: String? = nil
    
    public struct AttributeReference {
        static let id = "id"
        static let main = "main"
        static let weatherDescription = "description"
        static let icon = "icon"
    }
    
    convenience init(withID id: Int?, main: String?, description: String?, icon: String?) {
        self.init()
        
        self.id = id
        self.main = main
        self.weatherDescription = description
        self.icon = icon
    }
}
