//
//  OWTemperature.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation

class OWTemperature: NSObject {
    
    var day: Float? = nil
    var night: Float? = nil
    var morn: Float? = nil
    var eve: Float? = nil
    var min: Float? = nil
    var max: Float? = nil
    
    public struct AttributeReference {
        static let day = "day"
        static let night = "night"
        static let morn = "morn"
        static let eve = "eve"
        static let min = "min"
        static let max = "max"
    }
    
    convenience init (withDay day: Float?, night: Float?, morn: Float?, eve: Float?, min: Float?, max: Float?) {
        self.init()
        self.day = day
        self.night = night
        self.morn = morn
        self.eve = eve
        self.min = min
        self.max = max
    }
}
