//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class WeatherCell: UICollectionViewCell {
    
    var weatherIconRequest: Alamofire.Request?
    
    @IBOutlet weak var iconImageView: UIImageView?
    
    @IBOutlet weak var maxLabel: UILabel? {
        didSet {
            self.maxLabel?.font = UIFont.weatherCellTempFont()
        }
    }
    @IBOutlet weak var minLabel: UILabel? {
        didSet {
            self.minLabel?.font = UIFont.weatherCellTempFont()
        }
    }
    
    @IBOutlet weak var dateLabel: UILabel? {
        didSet {
            self.dateLabel?.font = UIFont.weatherCellDateFont()
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel? {
        didSet {
            self.descriptionLabel?.font = UIFont.weatherCellDescriptionFont()
            self.descriptionLabel?.textColor = UIColor.lightGray
        }
    }
    
    public func update(withWeatherForecast forecast: OWWeatherForecast?) {
        guard let forecast = forecast else {
            return
        }
        
        // date
        if let date = forecast.dateTimestamp {
            self.dateLabel?.text = Date.formatWeatherDate(date: date)
        } else {
            self.dateLabel?.text = String.Empty
        }
        
        // temperature
        if let temperature = forecast.temperature {
            if let max = temperature.max {
                self.maxLabel?.text = String.degreesFormat(forTemperature: max)
            }
            
            if let min = temperature.min {
                self.minLabel?.text = String.degreesFormat(forTemperature: min)
            }
        }
        
        // weather
        if let weather = forecast.weather.first {
            // weather icon
            if let code = weather.icon {
                self.requestWeatherIcon(forCode: code)
            }
            
            if let main = weather.main {
                self.dateLabel?.text?.append(" - \(main)")
            }
            
            if let desc = weather.weatherDescription {
                self.descriptionLabel?.text = desc
            }
        }
    }
    
    // MARK: - Weather Icon Request
    
    private func requestWeatherIcon(forCode code: String) {
        guard let weatherIconURL = URL(string: "http://openweathermap.org/img/w/\(code).png") else {
            return
        }
        
        self.weatherIconRequest = Alamofire.request(weatherIconURL).response { [weak self] response in
            guard let data = response.data else {
                return
            }
            self?.iconImageView?.image = UIImage(data: data, scale: 1)
        }
    }
    
    // MARK: - Reuse
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.weatherIconRequest?.cancel()
    }
}
