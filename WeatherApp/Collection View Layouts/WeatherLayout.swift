//
//  WeatherLayout.swift
//  WeatherApp
//
//  Created by Dom Chapman on 12/03/2017.
//  Copyright © 2017 Dom Chapman. All rights reserved.
//

import Foundation
import UIKit

class WeatherLayout: UICollectionViewLayout {
    private var cache = [UICollectionViewLayoutAttributes]()
    
    var contentHeight: CGFloat = 0
    var yOffset: CGFloat = 0
    let cellHeight: CGFloat = 66
    
    override func prepare() {
        guard let collectionView = self.collectionView else {
            return
        }
        
        if cache.isEmpty {
            // iterate over all sections in collection view
            for section in 0 ..< collectionView.numberOfSections {
                
                // iterate over all items in section
                for index in 0 ..< collectionView.numberOfItems(inSection: section) {
                    
                    let indexPath = NSIndexPath(item: index, section: section)
                    let width: CGFloat = collectionView.frame.width
                    let frame = CGRect(x: CGFloat(0), y: yOffset, width: width, height: self.cellHeight)
                    
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
                    attributes.frame = frame
                    cache.append(attributes)
                    
                    contentHeight = max(contentHeight, frame.maxY)
                    yOffset = yOffset + self.cellHeight
                }
            }
        }
    }
    
    override var collectionViewContentSize: CGSize {
        guard let collectionView = self.collectionView else {
            return CGSize(width: 0, height: 0)
        }
        return CGSize(width: CGFloat(collectionView.frame.width), height: self.contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                layoutAttributes.append(attributes)
            }
        }
        return layoutAttributes
    }
}
